/* FIXME: refactor the check response stuff */
/* FIXME: try to be doxygen conform */
/* FIXME: refactor a lot ;) */
using Gee;

namespace Antidote {

	/* FIXME: use RFC Errors! */
	errordomain IMAPError {
		CONNECTION,
		LOGIN,
		LIST
	}

	/* message struct */
	struct Message {
		public string from;
		public string to;
		public string date;
		public string subject;
		public string full_header;
	}

	class IMAP {
		public IMAP () {
			logger = new Logger();
			//FIXME: add if debug or something equal later!
			logger.name= "Antidote:IMAP";
			logger.log_level= logger.LogLevel.DEBUG;
		}

		private Logger logger;
		private SocketConnection conn;
		private SocketClient client = new SocketClient ();
		private DataInputStream response;
		private uint16 _q_id = 0;

		public string host { get; set; }
		public uint16 port { get; set; }
		public string username { get; set; }
		public string password { get; set; }
		public bool ssl { get; set; }

		public string next_q_id() {
			return "a" + (_q_id++).to_string(); //overflows on purpose ;)
		}

		/*
		 * Establish a connection to the server and keep it.
		 * Throw error if connection cannot be established.
		 */
		public void connect() throws GLib.Error, IMAPError {
			var resolver = Resolver.get_default();
			var addresses = resolver.lookup_by_name(host, null);
			var address = addresses.nth_data(0);

			logger.log(logger.LogLevel.DEBUG,@"Resolved $host to $address\n");

			if(ssl) {
				client.set_tls(ssl);
				client.set_tls_validation_flags(TlsCertificateFlags.UNKNOWN_CA);
				logger.log(logger.LogLevel.DEBUG,"using ssl\n");
			}
			conn = client.connect(new InetSocketAddress (address, port));

			logger.log(logger.LogLevel.DEBUG,@"Connected to $host\n");

			/* Check response */
			response = new DataInputStream (conn.input_stream);
			var status_line = response.read_line(null);
			if (!status_line.contains("OK")) {
				throw new IMAPError.CONNECTION(status_line);
			}
			else {
				logger.log(logger.LogLevel.DEBUG,status_line+"\n");
			}
		}

		/* simple imap command wrapper */
		private string imap_command(string command) throws GLib.Error, IMAPError {
				var ret = "";
				var q_id = next_q_id();

				var message = @"$q_id $command\n";
				conn.output_stream.write(message.data);
//				logger.log(logger.LogLevel.DEBUG,message);

				/* Check response */
				var status_line = response.read_line(null);
				if (status_line.contains(q_id + " NO") || status_line.contains("BAD"))
					throw new IMAPError.LIST(status_line);

				ret = status_line+"\n";

				/* The command has a on line return value - we must return here! */
				if (ret.contains(q_id + " OK"))
					return ret;

				//FIXME: NO responses?
				while (!(status_line = response.read_line(null)).contains(q_id + " OK")) {
					ret = ret + status_line+"\n";
				}

//				logger.log(logger.LogLevel.DEBUG,ret);

				return ret;
		}

		/* login into account throw imap error LOGIN */
		public void login() throws GLib.Error, IMAPError {
			imap_command(@"LOGIN $username $password");
		}

		/* list dir */
		public string list(string what) throws GLib.Error, IMAPError {
			return imap_command(@"LIST \"\" \"$what\"");
		}

		/* examine dir */
		public string examine(string what) throws GLib.Error, IMAPError {
			return imap_command(@"EXAMINE $what");
		}

		/* fetch foo as string */
		public string fetch(string start_uid,string end_uid,string what) throws GLib.Error, IMAPError {
			return imap_command(@"FETCH $start_uid:$end_uid $what");
		}


		/* get thread */
		public string thread(string algorithm,string char_set,string what) throws GLib.Error, IMAPError {
			return imap_command(@"THREAD $algorithm $char_set $what");
		}

		/* get headers as hash map */
		public HashMap<int,Message?> get_headers(string start_uid,string end_uid) {
			var regex_to = new Regex ("""^To:*""");
			var regex_from = new Regex ("""^From:*""");
			var regex_subject = new Regex ("""^Subject:*""");
			var regex_date = new Regex ("""^Date:*""");
			var regex_uid = new Regex ("""HEADER""");

			//FIXME: this code may take very long to complete!!!
			var map = new HashMap<int,Message?> ();
			string all = imap_command(@"FETCH $start_uid:$end_uid RFC822.HEADER");
			var list = all.split("\n)");

			foreach (string s in list) {
				int uid= 0;
				Message tmp = Message();
				var lines = s.split("\n");
				foreach (string line in lines ){
					if (regex_uid.match(line)) {
						var parts = line.split(" ");
						uid=parts[1].to_int();
					}
					else if (regex_to.match(line))
						tmp.to = line.replace("To: ","").strip();
					else if (regex_from.match(line))
						tmp.from = (string)line.replace("From: ","").strip();
					else if (regex_subject.match(line))
						tmp.subject = (string)line.replace("Subject: ","").strip();
					else if (regex_date.match(line))
						tmp.date = line.replace("Date: ","").strip();
				}

				/* only push the entry if we found a header in the string */
				if(uid != 0) {
					tmp.full_header = s;
					map.set (uid,tmp);
				}
			}

			return map;
		}
	}
}
